use <boltnut/M3.scad>;
use <roundcube/cubeX.scad>

$fn = 70;

leverRadius = 8.5/2;
rigRadius = 10/2;
tolerance = 0.12;


angle = 57;

width = 40;
height = 40;
thicknessHalf = 9;

leverOffset = 12;
nutHeight = 1.6;
d_head_m3 = 5.9;

union()
{
    leverClamp("bolt");
    
    translate([width/2 + 10 + width/2,0,0])
    scale([-1,1,1])
        leverClamp("nut");
        
    
    translate([0,height + 10, 0])
    {
        topClamp("bolt");

        translate([width/2 + 10 + width/2,0,0])
            topClamp("nut");
    }
}



module topClamp(boltOrNut = "bolt")
{
    rigHeight = rigRadius*2 + leverOffset*2;
    rigWidth = rigRadius*2 + 8*2;
    rigHeightD = height - rigHeight;
    nutZ = boltOrNut == "nut" ? (-thicknessHalf/2 + nutHeight) : (-thicknessHalf/2 + nutHeight/2);
    difference()
    {
        union()
        {
            translate([0,rigHeightD/2,0])
                cubeX([width, rigHeight, thicknessHalf], center=true);
            cubeX([rigWidth, height, thicknessHalf], center=true);
        }
        
        translate([0,rigHeightD/2, rigRadius])
        rotate([0,90,0])
        cylinder(h=width+1, r=rigRadius+tolerance, center=true);

        translate([0,0,rigRadius])
        rotate([90,0,0])
        cylinder(h=height+1, r=rigRadius+tolerance, center=true);

        translate([-width/4,-height/2 + leverOffset/2 + rigHeightD,0])
        {
            cylinder(h=thicknessHalf, r=1.4, center=true);
            translate([0,0,nutZ])
            {
                if (boltOrNut == "nut") NutM3( h=nutHeight);
                if (boltOrNut == "bolt") cylinder(h=nutHeight, r=d_head_m3/2, center=true );
            }
        }
        translate([width/4,-height/2 + leverOffset/2 + rigHeightD,0])
        {
            cylinder(h=thicknessHalf, r=1.4, center=true);
            translate([0,0,nutZ])
            {
                if (boltOrNut == "nut") NutM3( h=nutHeight);
                if (boltOrNut == "bolt") cylinder(h=nutHeight, r=d_head_m3/2, center=true );
            }
        }

        translate([width/4,height/2 - leverOffset/2,0])
        {
            cylinder(h=thicknessHalf, r=1.4, center=true);
            translate([0,0,nutZ])
            {
                if (boltOrNut == "nut") NutM3( h=nutHeight);
                if (boltOrNut == "bolt") cylinder(h=nutHeight, r=d_head_m3/2, center=true );
            }
        }
        translate([-width/4,height/2 - leverOffset/2,0])
        {
            cylinder(h=thicknessHalf, r=1.4, center=true);
            translate([0,0,nutZ])
            {
                if (boltOrNut == "nut") NutM3( h=nutHeight);
                if (boltOrNut == "bolt") cylinder(h=nutHeight, r=d_head_m3/2, center=true );
            }
        }

    }
}




module leverClamp(boltOrNut = "bolt")
{
    difference()
    {
        cubeX([width, height, thicknessHalf], center=true);
        
        translate([0,-height/2 + leverRadius + leverOffset, leverRadius])
        rotate([0,90,0])
        {
            cylinder(h=width+1, r=leverRadius+tolerance, center=true);
            
            translate([0,18,(leverRadius-rigRadius)])
            rotate([-angle,0,0])
                cylinder(h=width, r=rigRadius+tolerance, center=true);
        }
        
        nutZ = boltOrNut == "nut" ? (-thicknessHalf/2 + nutHeight) : (-thicknessHalf/2 + nutHeight/2);
        
        translate([0,-height/2 + leverOffset/2,0])
        {
            cylinder(h=thicknessHalf, r=1.4, center=true);
            translate([0,0,nutZ])
            {
                if (boltOrNut == "nut") NutM3( h=nutHeight);
                if (boltOrNut == "bolt") cylinder(h=nutHeight, r=d_head_m3/2, center=true );
            }
        }

        translate([width/4,height/2 - leverOffset,0])
        {
            cylinder(h=thicknessHalf, r=1.4, center=true);
            translate([0,0,nutZ])
            {
                if (boltOrNut == "nut") NutM3( h=nutHeight);
                if (boltOrNut == "bolt") cylinder(h=nutHeight, r=d_head_m3/2, center=true );
            }
        }
        translate([-width/3,height/2 - leverOffset*0.7,0])
        {
            cylinder(h=thicknessHalf, r=1.4, center=true);
            translate([0,0,nutZ])
            {
                if (boltOrNut == "nut") NutM3( h=nutHeight);
                if (boltOrNut == "bolt") cylinder(h=nutHeight, r=d_head_m3/2, center=true );
            }
        }

    }
}






use <boltnut/M3.scad>;

tireCSDiameter = 0;	// O-ring cross-sectional diameter (CS) -- How thick is the o-ring rubber?
tireID = 85;			// O-ring Internal diameter (ID) -- How wide is the inside opening? 
tireStretch = 1.00;	// O-ring circumferential stretch percentage (usually 1 + 0-5%) -- How much to 
					// 		you want to stretch it to get it on? 

wheelWidth = 16;		// The width (or thickness) of the the wheel.
rimHeight = 4;		// The height of the rim portion of the wheel. (Must be greater than tire CS radius.) 
numberOfSpokes = 8;	// Number of "spokes." Leave this at three if you're doing the biohazard design
spokeWidth = 9;		// This is how wide each spoke is.
curvature = .66;		// This defines how curvey the spokes are. >0, but <=1, where 1 is a half circle
reverse = false;		// Setting this to "true" reverses the direction of the spokes

hubDiameter = 0;				// The diameter of the hub portion of the wheel
hubHeight = 0;				// The height of the hub
shaftDiameter = 0;			// The diameter of the motor shaft
setScrewDiameter = 0;			// The diameter of the set screw. 3 is the default for an M3 screw. 
setScrewNutDiameter = 0;	// The "diameter" of the captive nut, from flat to flat (the "in-diameter")
setScrewNutThickness = 0;	// The thickness of the captive nut
setScrewCount = 0;			// The number of set screws/nuts to render, spaced evenly around the shaft 
hubZOffset = -wheelWidth/2;		// The Z position of the hub, negative numbers from the surface of the wheel 
baseFilletRadius = 0;			// The radius of the fillet (rounded part) between the hub and wheel. 
topFilletRadius = 0;			// The radius of the fillet (rounded part) at the top of the hub. 
chamferOnly = false;			// Set to true to use chamfers (straight 45-degree angles) instead of fillets. 

$fn=128;						// Default quality for most circle parts. 


toleranceInnerR = 0.12;
bearingRadius = 11 + toleranceInnerR;
bearingWall = 3;
bearingOuterRadius = bearingWall + bearingRadius;
bearingHeight = 7;
bearingCap = (wheelWidth - bearingHeight) / 2;
bearingAxleRadius = 4;
bearingAxleOuterRadius = bearingAxleRadius + 1.8;
bearingCapHoleRadius = bearingRadius - 3.2;

alpha = 42;
holeOffset = 0;
threadR = 1.42;
flapR = 4;


/////////////////////////////////////////////////////////////////////////////
// Calculated values... 
/////////////////////////////////////////////////////////////////////////////

// Let's get some basic math out of the way first.
// Not all of these are necessary, but many are helpful to know and will be echoed to the user. 

// Some basic tire geometry
innerDiameter = tireID; 
centerDiameter = tireID + tireCSDiameter;
outerDiameter = tireID + ( tireCSDiameter *2 );
innerCircumference = innerDiameter*PI;
centerCircumference = centerDiameter*PI;
outerCircumference = outerDiameter*PI;

// Stretched tire geometry
centerCircumferenceStretched = centerCircumference * tireStretch;
centerDiameterStretched = centerCircumferenceStretched/PI; 
innerDiameterStretched = centerDiameterStretched - tireCSDiameter;
outerDiameterStretched = centerDiameterStretched + tireCSDiameter;
innerCircumferenceStretched = innerDiameterStretched * PI;
outerCircumferenceStretched = outerDiameterStretched * PI;

// Wheel geometry.
wheelDiameter =  centerDiameterStretched;
wheelRadius = wheelDiameter / 2;
tireRadius = tireCSDiameter / 2;
hubRadius = hubDiameter / 2;


/////////////////////////////////////////////////////////////////////////////
// Report some basic information to the user...  
/////////////////////////////////////////////////////////////////////////////

echo ( str("Bearing cap is ",bearingCap) );

// Here's a helper function that will round a value to two decimal places. 
function round2( value ) = round(value * 100) / 100;


/////////////////////////////////////////////////////////////////////////////
// Render the wheel...  
/////////////////////////////////////////////////////////////////////////////

difference()
{

    union() {
        rim(wheelWidth, rimHeight,wheelDiameter, tireCSDiameter);

        difference() 
        { 
            spokes(wheelDiameter - (rimHeight*2), wheelWidth, numberOfSpokes, spokeWidth, curvature, reverse);
            
            // cut space for bearing hub 
            cylinder( h=wheelWidth+1, r=bearingOuterRadius, center=true); 
        }

        bearingHub();	
    }

    // cut space for top flaps
    translate([-bearingOuterRadius*cos(alpha), bearingOuterRadius*sin(alpha), wheelWidth/2-bearingCap/2])
        cylinder( h=bearingCap, r=flapR, center=true); 
    translate([bearingOuterRadius*cos(alpha), -bearingOuterRadius*sin(alpha), wheelWidth/2-bearingCap/2])
        cylinder( h=bearingCap, r=flapR, center=true); 
    
    // cut nuts tunnels at bottom
    tunnelHeight = 6;
    translate([-bearingOuterRadius*cos(alpha), bearingOuterRadius*sin(alpha), -wheelWidth/2+tunnelHeight/2])
        NutM3( h=tunnelHeight );
    translate([bearingOuterRadius*cos(alpha), -bearingOuterRadius*sin(alpha), -wheelWidth/2+tunnelHeight/2])
        NutM3( h=tunnelHeight );
}



translate([0,0, wheelWidth + 10])
{
    h_head_m3 = 2.3;
    d_head_m3 = 5.9;
    
    difference()
    {
        union()
        {
            cylinder( h=bearingCap, r=bearingRadius-toleranceInnerR, center=true); 
            
            translate([-bearingOuterRadius*cos(alpha), bearingOuterRadius*sin(alpha), 0])
            {
                difference()
                {
                    cylinder( h=bearingCap, r=flapR-toleranceInnerR, center=true); 
                    cylinder( h=bearingCap+1, r=threadR, center=true );
                    
                    translate([0,0,bearingCap/2-h_head_m3/2])
                        cylinder( h=h_head_m3, r=d_head_m3/2, center=true );
                    
                }
            }

            translate([bearingOuterRadius*cos(alpha), -bearingOuterRadius*sin(alpha), 0])
            {
                difference()
                {
                    cylinder( h=bearingCap, r=flapR-toleranceInnerR, center=true); 
                    cylinder( h=bearingCap+1, r=threadR, center=true );
                    
                    translate([0,0,bearingCap/2-h_head_m3/2])
                        cylinder( h=h_head_m3, r=d_head_m3/2, center=true );
                }
            }

        }
        
        cylinder( h=bearingCap, r=bearingCapHoleRadius, center=true); 
    }
}



module bearingHub() {
       
    union()
    {
        difference() {
            
            // outer bearing wall and flaps
            union(){
                translate([-bearingOuterRadius*cos(alpha), bearingOuterRadius*sin(alpha), -bearingCap/2])
                    cylinder( h=wheelWidth-bearingCap, r=flapR , center=true); 

                translate([bearingOuterRadius*cos(alpha), -bearingOuterRadius*sin(alpha), -bearingCap/2])
                    cylinder( h=wheelWidth-bearingCap, r=flapR , center=true); 


                cylinder( h=wheelWidth, r=bearingOuterRadius , center=true); 
            }
            
            // bearing inner space
            cylinder( h=wheelWidth+1, r=bearingRadius , center=true); 
            
            // thread holes
            translate([-(bearingOuterRadius+holeOffset)*cos(alpha), (bearingOuterRadius+holeOffset)*sin(alpha), 0])
                cylinder( h=wheelWidth+1, r=threadR , center=true); 

            translate([(bearingOuterRadius+holeOffset)*cos(alpha), -(bearingOuterRadius+holeOffset)*sin(alpha), 0])
                cylinder( h=wheelWidth+1, r=threadR , center=true); 

        }
        
        
        translate([0,0,-bearingHeight/2 - bearingCap/2])
        {
            difference()
            {
                cylinder( h=bearingCap, r=bearingRadius, center=true); 
                cylinder( h=bearingCap+1, r=bearingCapHoleRadius, center=true); 
                
            }
        }
        
    }
    
} 


/////////////////////////////////////////////////////////////////////////////
// Modules...  
/////////////////////////////////////////////////////////////////////////////

// The rim (the solid area between the spokes and tire)
module rim( width, height, diameter, tireDiameter ) {
	difference() { 
		// rim 
		cylinder( h=width, r=diameter/2, center=true );
	
		// punch out center
		cylinder(h=width+1, r=diameter/2 - height, center=true );

		// punch out tire
		tire( diameter, tireDiameter ); 
	}
}

// The tire, where "diameter" is the center-to-center diameter (not ID or OD)
module tire( diameter, tireDiameter ) {
	render() {
		rotate_extrude(convexity = 10)
			translate([diameter/2, 0, 0])
				circle(r = tireDiameter/2, $fn=20);
	}
}

// The spokes. This scales the design in spoke() to produce the spokes for the wheel. 
// Change this an the spoke() module in order to customize the spokes.  
module spokes( diameter, wheelWidth, number, spokeWidth, curvature, reverse ) {

	intersection() {
		cylinder( h=wheelWidth, r=diameter/2, center = true ); 

		for (step = [0:number-1]) {
		    rotate( a = step*(360/number), v=[0, 0, 1])
			spoke( wheelWidth, spokeWidth, (diameter/4) * 1/curvature, reverse );
		}
	}
}

// One spoke. I tinkered with the sizes to get the right look for the biohazard wheel, and then 
// used spokes() to scale it to the size of the wheel. Change both of these to customize the 
// wheel. Build the spoke in the negative direction along the X axis
// spokeWidth = how thick the spoke should be
// curvature = >0 but <=1, how curved the spoke should be
module spoke( wheelWidth, spokeWidth, spokeRadius, reverse=false ) {
	render() {
		intersection() {	
			translate ( [-spokeRadius, 0, 0] ) {
				difference() {
					cylinder( r=spokeRadius, h=wheelWidth, center=true ); 
					cylinder( r=spokeRadius-(spokeWidth/2), h=wheelWidth+1, center=true ); 
				}
			}
			if ( reverse ) 
				translate ( [-spokeRadius, -spokeRadius/2, 0] ) 
					cube( [spokeRadius*2,spokeRadius,wheelWidth+1], center=true ); 
			else 
				translate ( [-spokeRadius, spokeRadius/2, 0] ) 
					cube( [spokeRadius*2,spokeRadius,wheelWidth+1], center=true ); 
		}
	}
}

// This is the captive nut module I use in several of my designs. 
module captiveNut( inDiameter=5.4, thickness=2.3, setScrewHoleDiameter=3, 
	depth=10, holeLengthTop=5, holeLengthBottom=5 )
{
	side = inDiameter * tan( 180/6 );

	render()
	union() {
		for ( i = [0 : 2] ) {
			rotate( i*120, [0, 0, 1]) 
				cube( [side, inDiameter, thickness], center=true );
		}
	
		translate([depth/2,0,0]) 
			cube( [depth, inDiameter, thickness], center=true );
	
		translate([0,0,-(thickness/2)-holeLengthBottom]) 
			cylinder(r=setScrewHoleDiameter/2, h=thickness+holeLengthTop+holeLengthBottom, $fn=15);
	}
}




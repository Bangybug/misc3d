use <boltnut/M3.scad>;
use <boltnut/Polygon.scad>;

// --- wheel settings ---
wheelWidth = 16;
toleranceInnerR = 0.12;
bearingRadius = 11 + toleranceInnerR;
bearingWall = 3;
bearingOuterRadius = bearingWall + bearingRadius;
bearingHeight = 7;
bearingCap = (wheelWidth - bearingHeight) / 2;
bearingAxleRadius = 4;
bearingAxleOuterRadius = bearingAxleRadius + 1.8;
bearingCapHoleRadius = bearingRadius - 3.2;
// ---

wheelRadius = 85/2;
bearingInnerRadius = 4;
bearingInnerRingWidth = 0.8;
coneHeight = 2;

coneSmallR = bearingInnerRadius - toleranceInnerR;
coneBigR = bearingInnerRadius + bearingInnerRingWidth;
coneBigR1 = 9.5 * 0.5 + 1.5; // just some arbitrary number that fits

axleInnerBearing = 2; // inside bearing ring

sleeveAxleWidth = 20;
sleeveAxleBlockingRing = 2; // blocking ring for the axle
sleeveAxleRibHeight = 2;
sleeveAxleRibThick = 1.5;

axleDiameter = 9.5;

echo(str("Minimum thread length = ",bearingCap*2+bearingHeight));



$fn = 120;

union()
{
    sleeve(true, bearingCap + coneHeight + axleInnerBearing);
    
    translate([coneBigR*2+5,0,+bearingCap/2])
    {
        sleeveAxle(0 + coneHeight + axleInnerBearing);
        
        
        translate([-30,0, -20])
            stand();
    }
}


module stand()
{
    standOutsetR = axleDiameter * 0.5 + 2; // will make hollow cyl with 2mm wall
    standTubeFitHeight = coneBigR1*2;
    axleTubeThreadDist = 3; 
    tubeAxleHeight = 20;
    standTubeHeight = tubeAxleHeight - standTubeFitHeight/2 + axleTubeThreadDist;
    h = sleeveAxleRibHeight;
    
    difference()
    {
        union()
        {
            difference()
            {
                union()
                {
                    difference()
                    {
                        rotate([90,0,0])
                            cylinder(h=standTubeFitHeight, r=standOutsetR, center=true);
                        
                        translate([0,0, -standOutsetR + h/2])
                            cube([standOutsetR*2, coneBigR1*2, h], center=true);
                    }
                        
                        
                    translate([0,0,-standOutsetR + h/2])
                    union()
                    {
                        translate([0,-toleranceInnerR*2,0])
                            cylinder(h=h, r=coneBigR1-sleeveAxleRibThick-toleranceInnerR, center=true);
                        cube( [coneBigR1-toleranceInnerR,coneBigR1*2,h], center=true ); 
                    }
                }
                
                // cut thread
                translate([0,-toleranceInnerR*2,0])
                {
                    cylinder(h=standOutsetR*2, r=1.4, center=true);
                
                    h_head_m3 = 3;
                    translate([0,0,standOutsetR])
                        NutM3( h=h_head_m3);
                }
                
            }
            
            // append stand
            translate([0,standTubeFitHeight/2+standTubeHeight/2,0])
            rotate([90,0,0])
                cylinder(h=standTubeHeight, r=standOutsetR, center=true);
        }
        
        
        // cut axle inset
        translate([0, tubeAxleHeight/2 + axleTubeThreadDist,0])
        rotate([90,0,0])
            cylinder(h=tubeAxleHeight, r=axleDiameter/2 + toleranceInnerR, center=true);

    }

}


module sleeve(boltOrNut = false, sleeveWidth, noBoltNut=false)
{
    cylinderLen = sleeveWidth - coneHeight - axleInnerBearing;
    difference()
    {
        translate([0,0, -coneHeight/2 - axleInnerBearing + sleeveWidth/2])
        {
            union() 
            {
                // iniside bearing
                translate([0,0,coneHeight/2 + axleInnerBearing/2])
                    cylinder(h=axleInnerBearing, r=coneSmallR, center=true);

                // cone
                cylinder(coneHeight,coneBigR, coneSmallR, center=true);

                // inside bearing cap
                translate([0,0,-coneHeight/2 - cylinderLen/2])
                    cylinder(h=cylinderLen, r=coneBigR, center=true);
                    
            }
        }
        
        // cut thread
        cylinder(h=sleeveWidth, r=1.4, center=true);

        if (!noBoltNut)
        {
            if (boltOrNut)
            {
                h_head_m3 = 3;
                d_head_m3 = 5.9;
                translate([0,0,-sleeveWidth/2 + h_head_m3/2])
                    cylinder(h=h_head_m3, r=d_head_m3/2, center=true );
            }
            else
            {
                h_head_m3 = 4;
                translate([0,0,-sleeveWidth/2 + h_head_m3/2])
                    NutM3( h=h_head_m3);
            }
        }
    }   
}

module sleeveAxle(sleeveWidth)
{   
    union()
    {
        sleeve(false, sleeveWidth, true);

        translate([0,0,-sleeveWidth/2])
        {
        
            difference() // xx
            {
                union()
                {
                    // 2nd cone
                    translate([0,0,-bearingCap/2])
                    cylinder(bearingCap, coneBigR1, coneBigR, center=true);
                    
                    // outset - blocking ring
                    translate([0,0,-bearingCap - sleeveAxleBlockingRing/2])
                    cylinder(h=sleeveAxleBlockingRing, r=coneBigR1, center=true);
                    
                    // outset - aligning rib
                    translate([0,0,-bearingCap - sleeveAxleBlockingRing - sleeveAxleRibHeight/2])
                    {
                        difference()
                        {
                            cylinder(h=sleeveAxleRibHeight, r=coneBigR1, center=true);
                            cylinder(h=sleeveAxleRibHeight, r=coneBigR1-sleeveAxleRibThick, center=true);
                            cube( [coneBigR1,coneBigR1*2,sleeveAxleRibHeight], center=true ); 
                        }
                    }
                }
                
                // cut thread
                h = bearingCap + sleeveAxleBlockingRing + sleeveAxleRibHeight;
                translate([0,0,-h/2])
                    cylinder(h=h, r=1.4, center=true);
            }
        }
    }
}
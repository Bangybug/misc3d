use <roundcube/cubeX.scad>

$fn = 128;

rigHeight = 170;
rigWidth = 17;
rigThick = 4;

ribThick = 1.2;

tolerance = 0.12;


difference()
{
    union()
    {
        cubeX([rigHeight, rigWidth, rigThick], center=true);
        
        translate([0, -6,rigThick/2 + ribThick/2])
            cube( [rigHeight, ribThick, ribThick], center=true);

        translate([0, 6,rigThick/2 + ribThick/2])
            cube( [rigHeight, ribThick, ribThick], center=true);
    }

    
    holeStep = 16;
    holesCount = 2;
    
    for(i = [0 : 1 : holesCount])
    {
        translate([-i*holeStep,0, 0])
            cylinder(h=20, r=4, center=true);
    }

    for(i = [1 : 1 : holesCount])
    {
        translate([i*holeStep,0, 0])
            cylinder(h=20, r=4, center=true);
    }

    translate([-rigHeight/2 + 5, 0,0])
        threadHoles();

    translate([rigHeight/2 - 5, 0,0])
        threadHoles(step=-1);
        

}

module threadHoles(step=1)
{
    holeStep = 8 + 1.4;
    holesCount = 4;
    
    for(i = [0 : step : (step<0 ? -1 : 1)*holesCount])
    {
        translate([i*holeStep,0, 0])
            cylinder(h=10, r=1.4, center=true);
    }
}


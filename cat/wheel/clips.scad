use <boltnut/M3.scad>;
use <boltnut/Polygon.scad>;
use <gear/parametric_involute_gear_v5.0.scad>
use <bezier/BezierScad.scad>

axleDiameter = 9.5;
leverDiameter = 8.5;
toleranceR = 0.12;

sleeveHeight = 24;
sleeveBottomHeight = 4;
sleeveInR = axleDiameter * 0.5;
sleeveWallThick = 2;
sleeveOutR = sleeveInR + sleeveWallThick;

flapHole = 2.6; // in x direction
flapDistance = 1;
flapThick = 3.3;
flapHeight = 20;
flapThreadK = 0.1;

cogThick = 3.4;
cogHubThick = sleeveOutR - flapDistance/2 - flapThick;
cogHubR = 7;
cogHubThreadOffset = 0;

cogLeverCapThick = cogThick + 3;
cogLeverCapInset = 1.6;
cogLeverThick = 3;
cogLeverWallWidth = 3;
cogCircularPitch =220;
cogNumberOfTeeth = 20;
cogR = cogOuterR(cogNumberOfTeeth,cogCircularPitch);
cogCircularPitchTolerated = cogCircularPitch(cogNumberOfTeeth, cogR-toleranceR*4.4);
cogCapR = cogR + cogLeverWallWidth;

estimateBoltWidth = flapThick * 2 + flapDistance + cogHubThick + cogLeverCapThick + cogLeverThick - cogLeverCapInset - 1.2;
threadOffset = 5;


echo(str("Estimated bolt width = ",estimateBoltWidth));


$fn = 128;

union()
{
    axleSleeve(createHexInset = true);
    offset = 7;
    
    translate([
        -sleeveOutR -(sleeveOutR/2+flapHeight/4)/2, 
        -flapDistance/2 - flapThick - cogHubThick/2 - offset,
        -sleeveHeight*flapThreadK + cogHubThreadOffset])
    {
        cog();
        
        translate([ 0, -cogHubThick/2 - cogLeverCapThick/2 - offset, 0])
        {
            cogLeverCap();

            translate([ 0, -cogLeverCapThick/2 - cogLeverThick/2 + cogLeverCapInset - offset, 0])
                cogLeverClip();
        }
    }
    
    translate([0,60,0])
        arc();
}


module arc(arcWidth = 80, arcHeight = 80, thicknessZ = 4, thicknessX = 17)
{
    clipR = leverDiameter/2 + toleranceR;
    clipOuterR = clipR + 2;

    points = [
        [0,0],
        [0, arcHeight*0.55],
        [arcWidth*0.35, arcHeight*0.75],
        [arcWidth*0.9, arcHeight],
        [arcWidth,arcHeight],
    ];
 
    union()
    {
        difference()
        {    
            BezWall( points, width = thicknessX, height = thicknessZ, steps = 32, centered = true );
            
            BezModuleAlongPath( points, steps = 32 );
        }

        //translate([arcWidth,arcHeight, thicknessX/4 - thicknessZ/2])
        //5c x    sphere(r=thicknessX/2);


        translate([arcWidth,arcHeight, 0])
            cylinder(h=4, r=thicknessX/2);

        /*
        translate([arcWidth + 10,arcHeight, 20/2])
        {
            rotate([0,0,180])
                axleSleeve(sleeveOutR = clipOuterR, sleeveInR=clipR, flapThreadK=0, sleeveHeight=20, sleeveBottomHeight=0,
                    flap1BoltNut="nut", flap2BoltNut="bolt", flapHeight=13);
        }
        */

    }
}


module BezModuleAlongPath( 
  ctlPts, 
  steps = 16,
  stepDelta = 6) 
{
  hodoPts = hodograph(ctlPts);
  for(step = [stepDelta+1 : stepDelta : steps-stepDelta-1])
  {
    t0 = (step-1)/(steps-1);
    p0 = PerpAlongBez(t0, ctlPts, dist = 0, hodograph = hodoPts);
    mySphere( p0 );
  } 
}


module mySphere(p) 
{
    translate([p.x, p.y, 0])
        cylinder(r=4, h=5);
}


module axleSleeve(
    sleeveOutR=sleeveOutR, sleeveInR=sleeveInR,  sleeveHeight=sleeveHeight, sleeveBottomHeight=sleeveBottomHeight,
    flapThreadK=flapThreadK, flap1BoltNut="none", flap2BoltNut="bolt", createHexInset=false, flapHeight=flapHeight)
{
    difference() 
    {
        union()
        {
            cylinder(h=sleeveHeight, r=sleeveOutR, center=true);
            
            translate([-sleeveOutR, -flapDistance/2 - flapThick/2 ,0])
                flap(boltOrNut=flap1BoltNut, flapThreadK=flapThreadK, sleeveHeight=sleeveHeight, flapHeight=flapHeight);
            translate([-sleeveOutR, flapDistance/2 + flapThick/2 ,0])
                flap(boltOrNut=flap2BoltNut, flapThreadK=flapThreadK, sleeveHeight=sleeveHeight, flapHeight=flapHeight);
            
            if (createHexInset)
            {
                flapFullHeight = sleeveOutR/2+flapHeight;
                
                difference()
                {
                    translate([-sleeveOutR-(flapFullHeight/2),-sleeveOutR,-sleeveHeight/2])
                        cube([sleeveOutR + (flapFullHeight/2),sleeveOutR-flapThick,sleeveHeight]);
                    
                    union()
                    {
                        translate([-sleeveOutR -(sleeveOutR/2+flapHeight/4)/2, -sleeveOutR/2, -sleeveHeight*flapThreadK])
                        rotate([90,0,0])
                        cylinder(h=sleeveOutR, r=1.4, center=true);
                        
                        translate([-sleeveOutR -(sleeveOutR/2+flapHeight/4)/2, -sleeveOutR + cogHubThick/2 + toleranceR, -sleeveHeight*flapThreadK])
                        rotate([90,30,0])
                        Hexagon( A=cogHubR*0.55+toleranceR, h=cogHubThick+toleranceR*2 );
                    }
                }
               
            }
        }       

        // axle hole
        translate([0,0, -sleeveBottomHeight/2])
            cylinder(h=sleeveHeight - sleeveBottomHeight, r=sleeveInR, center=true);
    
        // flaps hole
        translate([-sleeveOutR + flapHole/2,0,0])
            cube([flapHole, flapDistance, sleeveHeight+10], center=true);
    }
}


module flap(boltOrNut = "bolt", sleeveOutR=sleeveOutR, flapThreadK=flapThreadK, sleeveHeight=sleeveHeight, flapHeight=flapHeight)
{    
    difference() 
    {
        cube([sleeveOutR/2+flapHeight, flapThick, sleeveHeight], center=true);

        translate([-(sleeveOutR/2+flapHeight/4)/2, 0, -sleeveHeight*flapThreadK])
        rotate([90,0,0])
        {
            cylinder(h=flapThick*2, r=1.4, center=true);
            if (boltOrNut == "bolt")
            {
                h_head_m3 = 1.2;
                d_head_m3 = 5.9;
                translate([0,0,-flapThick/2 + h_head_m3/2 - 0.1])
                    cylinder(h=h_head_m3, r=d_head_m3/2, center=true );
            }
            else if (boltOrNut == "nut")
            {
                h_head_m3 = 1;
                translate([0,0,flapThick/2])
                    NutM3( h=h_head_m3);
            }
        }
    }
}

module cog()
{
    union()
    {    
        rotate([90,30,0])
        translate([0,-cogHubThreadOffset,0])
        difference()
        {
            Hexagon( A=cogHubR*0.55, h=cogHubThick );
            cylinder( h=cogHubThick, r=1.4, center=true);
        }
        
        translate([0,-cogHubThick/2 - cogThick/2,0])
        rotate([90,0,0])
        difference()
        {
            translate([0,0,-cogThick/2])
            gear (circular_pitch=cogCircularPitchTolerated,
                number_of_teeth = cogNumberOfTeeth,
                gear_thickness = cogThick,
                rim_thickness = cogThick,
                hub_thickness = cogThick,
                hub_diameter = 0,
                bore_diameter = 0,
                circles=0);

            translate([0,-cogHubThreadOffset,0])
                cylinder(h=cogThick+0.1, r=1.4, center=true);
        }
        
 


    }
    
}


module cogLeverCap() 
{
    difference()
    {
        rotate([90,0,0])
        difference() 
        {        
            cylinder(h=cogLeverCapThick, r=cogCapR, center=true);
            translate([0,0,-cogLeverCapThick/2])
            gear (circular_pitch=cogCircularPitch,
                number_of_teeth = cogNumberOfTeeth,
                gear_thickness = cogThick,
                rim_thickness = cogThick,
                hub_thickness = cogThick,
                pressure_angle = 35, // larger than the default 28 angle of the cog, to allow tolerance
                hub_diameter = 0,
                bore_diameter = 0,
                circles=0);

            cylinder(h=cogLeverCapThick, r=1.4, center=true);
        }
        
        translate([0,-cogLeverCapThick/2 - cogLeverThick/2 + cogLeverCapInset ,cogCapR/2 - threadOffset/2])
            cube([20 + toleranceR*2,cogLeverThick, cogCapR+threadOffset+toleranceR*2],center=true);
    }
    
}


module cogLeverClip() 
{
    clipR = leverDiameter/2;
    clipOuterR = clipR + 2;
    
    union()
    {
        union()
        {
            difference()
            {
                translate([0,0,cogCapR/2 - threadOffset/2])
                    cube([20,cogLeverThick, cogCapR+threadOffset],center=true);
                
                rotate([90,0,0])
                {
                    cylinder(h=cogLeverThick+0.1, r=1.4, center=true);
                    
                    h_head_m3 = 1;
                    translate([0,0,cogLeverThick/2])
                        NutM3( h=h_head_m3);                    
                }
            }
            
            translate([0,-clipOuterR/2, cogCapR - clipOuterR])
            difference()
            {
                cube([20,clipOuterR,2*clipOuterR],center=true);
                
                translate([0, -clipOuterR/2, clipOuterR/2])
                cube([20,flapDistance, flapHeight],center=true);
                
                translate([0,-clipOuterR/2,0])
                rotate([0,90,0])
                    cylinder( h=22, r=clipOuterR, center=true);
            }

        }
       
        translate([0,-clipOuterR, cogCapR - clipOuterR])
        rotate([0,90,0])
            axleSleeve(sleeveOutR = clipOuterR, sleeveInR=clipR, flapThreadK=0, sleeveHeight=20, sleeveBottomHeight=0,
                flap1BoltNut="nut", flap2BoltNut="bolt", flapHeight=13);
    }
}


function cogOuterR(number_of_teeth, circular_pitch) =
    (number_of_teeth * circular_pitch / 180)/2 + 1/(number_of_teeth / (number_of_teeth * circular_pitch / 180));

function cogCircularPitch(number_of_teeth, outerR) =
    outerR/(number_of_teeth/360 + 1/180);



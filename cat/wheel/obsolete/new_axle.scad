use <M3.scad>;

// --- wheel settings ---
wheelWidth = 16;
toleranceInnerR = 0.12;
bearingRadius = 11 + toleranceInnerR;
bearingWall = 3;
bearingOuterRadius = bearingWall + bearingRadius;
bearingHeight = 7;
bearingCap = (wheelWidth - bearingHeight) / 2;
bearingAxleRadius = 4;
bearingAxleOuterRadius = bearingAxleRadius + 1.8;
bearingCapHoleRadius = bearingRadius - 3.2;
// ---

wheelRadius = 85/2;
bearingInnerRadius = 4;
bearingInnerRingWidth = 0.8;
coneHeight = 2;

coneSmallR = bearingInnerRadius - toleranceInnerR;
coneBigR = bearingInnerRadius + bearingInnerRingWidth;

axleGapSpace = 2; // inside bearing ring
sleeveWidth = bearingCap + coneHeight + axleGapSpace;

sleeveAxleWidth = 20;
sleeveAxleOutset = 3;
axleDiameter = 9.5;

echo(str("Sleeve width = ",sleeveWidth,", outer radius = ", coneBigR));
echo(str("Minimum thread length = ",bearingCap*2+bearingHeight));



$fn = 50;

union()
{
    sleeve(true);
    
    translate([coneBigR*2+5,0,0])
        sleeveAxle();
}


module sleeve(boltOrNut = false)
{
    difference()
    {
        translate([0,0, -coneHeight/2 - axleGapSpace + sleeveWidth/2])
        {
            union() 
            {
                // iniside bearing
                translate([0,0,coneHeight/2 + axleGapSpace/2])
                    cylinder(h=axleGapSpace, r=coneSmallR, center=true);

                // cone
                cylinder(coneHeight,coneBigR, coneSmallR, center=true);

                // inside bearing cap
                translate([0,0,-coneHeight/2 - bearingCap/2])
                    cylinder(h=bearingCap, r=coneBigR, center=true);
                    
            }
        }
        
        // cut thread
        cylinder(h=sleeveWidth, r=1.4, center=true);

        if (boltOrNut)
        {
            h_head_m3 = 3;
            d_head_m3 = 5.9;
            translate([0,0,-sleeveWidth/2 + h_head_m3/2])
                cylinder(h=h_head_m3, r=d_head_m3/2, center=true );
        }
        else
        {
            h_head_m3 = 4;
            translate([0,0,-sleeveWidth/2 + h_head_m3/2])
                NutM3( h=h_head_m3);

        }
    }   
}

module sleeveAxle()
{
    axleOutsetR = axleDiameter * 0.5 + 2;
    union()
    {
        sleeve(false);
        
        translate([0,0,-sleeveWidth/2])
        {
            translate([0,0,-sleeveAxleOutset/2])
            difference()
            {
                cylinder(sleeveAxleOutset, axleOutsetR, coneBigR, center=true);
                translate([0,0,sleeveAxleOutset/2])
                    NutM3( h=sleeveAxleOutset);
            }
            
            translate([0,0,-sleeveAxleWidth/2 - sleeveAxleOutset])
            difference() 
            {
                cylinder(h=sleeveAxleWidth, r=axleOutsetR, center=true);
                cylinder(h=sleeveAxleWidth, r=axleDiameter/2+toleranceInnerR*0.9, center=true);
            }
        }
    }
}
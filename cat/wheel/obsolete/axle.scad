use <M3.scad>;

// --- wheel settings ---
wheelWidth = 16;
toleranceInnerR = 0.12;
bearingRadius = 11 + toleranceInnerR;
bearingWall = 3;
bearingOuterRadius = bearingWall + bearingRadius;
bearingHeight = 7;
bearingCap = (wheelWidth - bearingHeight) / 2;
bearingAxleRadius = 4;
bearingAxleOuterRadius = bearingAxleRadius + 1.8;
bearingCapHoleRadius = bearingRadius - 3.2;
// ---

wheelRadius = 85/2;
bearingInnerRadius = 4;
bearingInnerRingWidth = 0.8;
coneHeight = 2;
sleeveInsetWidth = 2;
axleGapSpace = 2;
axleWheelStandSpacing = 3;
axleStandWidth = 5;
axleHalfWidth = axleStandWidth + axleWheelStandSpacing + sleeveInsetWidth;


coneSmallR = bearingInnerRadius - toleranceInnerR;
coneBigR = bearingInnerRadius + bearingInnerRingWidth;

axleRibWidth = 2.4;
sleeveWidth = bearingCap + coneHeight + axleGapSpace;
sleeveInsetBorder = 1;
sleeveInsetRadius = coneBigR - sleeveInsetBorder;

standHeightWheelGap = 5;
axleNutTunnelHeight = 0.6;



echo(str("Full axle length = ",axleHalfWidth*2+bearingHeight-2));
echo(str("Sleeve width = ",sleeveWidth,", outer radius = ", coneBigR));
echo(str("Minimum thread length = ",bearingCap*2+bearingHeight));
echo(str("Estimated bolt length = ",bearingHeight + bearingCap*2+ axleStandWidth*2 + axleWheelStandSpacing*2 + 3 + 3 ));



$fn = 50;

union()
{
    //sleeve();
    
    translate([0,0,-sleeveWidth/2 - axleHalfWidth/2 - 5])
    {        
        axleStand();
    }
    
    
    
}


module stand()
{
    standHeight = wheelRadius + standHeightWheelGap;
    standWidth = (sleeveInsetRadius-toleranceInnerR)*2;
    sphereR = standWidth/2 * 0.9;
    
    sphereDistRK = 1.1;
    sphereCount = floor( (standHeight - coneBigR*2.2) / (sphereR * sphereDistRK * 2) );

    union() 
    {
        difference() 
        {    
            translate([0,standHeight/2,0])
                cube([standWidth, standHeight, axleStandWidth], center=true);
           
            /*
            translate([0,standHeight - sphereR*sphereDistRK,0])
            {
                for ( i = [0 : sphereCount] ) 
                {
                    translate([0, -i*sphereR*sphereDistRK*2, 0])
                        sphere( r=sphereR, center=true );
                }
            }
            */
        }
        
        translate([0,standHeight,0])
            standBar();
    }      
}


module standBar()
{
    standHeight = wheelRadius + standHeightWheelGap;
    standWidth = (sleeveInsetRadius-toleranceInnerR)*2;

    barLength = bearingHeight + bearingCap*2+ axleStandWidth*2 + axleWheelStandSpacing*2 + 2;
    barThickness = 7;
    
    translate([0, -barThickness/2, axleStandWidth/2 + barLength/2])
        cube([standWidth, barThickness, barLength],center=true);
}


module axleStand(boltOrNut=false)
{
    difference()
    {
        union()
        {
            // main axle
            cylinder(h=axleHalfWidth, r=sleeveInsetRadius-toleranceInnerR, center=true);
            
            /*
            intersection()
            {
                union()
                {
                    // h-ribs
                    difference()
                    {
                        cube([coneBigR*2,axleRibWidth-toleranceInnerR,axleHalfWidth], center=true);
                        
                        // make h-ribs angled
                        translate([-coneBigR - axleRibWidth/2,0,0])
                        rotate([0,-20,0])
                            cube([coneBigR,axleRibWidth-toleranceInnerR,axleHalfWidth*2], center=true);

                        translate([coneBigR + axleRibWidth/2,0,0])
                        rotate([0,20,0])
                            cube([coneBigR,axleRibWidth-toleranceInnerR,axleHalfWidth*2], center=true);                        
                    }
                    
                    // v-rib
                    difference()
                    {
                        translate([0,-coneBigR/2,0])
                        cube([axleRibWidth-toleranceInnerR,coneBigR, axleHalfWidth], center=true);
                        
                        // make it angled
                        translate([0,-coneBigR/2 - 4,0])
                        rotate([7,0,0])
                            cube([axleRibWidth-toleranceInnerR,coneBigR,axleHalfWidth*2], center=true);                        
                        
                    }
                }
                
                // round ribs a bit
                cylinder(h=axleHalfWidth, r=coneBigR, center=true);
            }
            */
            
            translate([0,0,-axleHalfWidth/2 + axleStandWidth/2])
                stand();
        }

        // cut thread
        cylinder(h=axleHalfWidth, r=1.4, center=true);


        // make convenient space either for a bolt head or its nut
        if (boltOrNut)
        {
            h_head_m3 = 0.6;
            d_head_m3 = 5.9;
            translate([0,0,-axleHalfWidth/2 + h_head_m3/2])
                cylinder(h=h_head_m3, r=d_head_m3/2, center=true );
        }
        else
        {
            translate([0,0,-axleHalfWidth/2 + axleNutTunnelHeight])
                NutM3( h=axleNutTunnelHeight);

        }
        
    }
}

module sleeve()
{
    difference()
    {
        translate([0,0, -coneHeight/2 - axleGapSpace + sleeveWidth/2])
        {
            union() 
            {
                // iniside bearing
                translate([0,0,coneHeight/2 + axleGapSpace/2])
                    cylinder(h=axleGapSpace, r=coneSmallR, center=true);

                // cone
                cylinder(coneHeight,coneBigR, coneSmallR, center=true);

                // inside bearing cap
                translate([0,0,-coneHeight/2 - bearingCap/2])
                    cylinder(h=bearingCap, r=coneBigR, center=true);
                    
            }
        }
        
        // cut thread
        cylinder(h=sleeveWidth, r=1.4, center=true);
        
        // cut inset
        translate([0,0,-sleeveWidth/2 + sleeveInsetWidth/2])
            cylinder(h=sleeveInsetWidth, r=sleeveInsetRadius, center=true);
            
        // cut holes in border for axle ribs
        translate([0,0,-sleeveWidth/2 + sleeveInsetWidth/2])
            cube ([coneBigR*2,axleRibWidth,2], center=true) ;
        
        translate([0,-coneBigR/2, -sleeveWidth/2 + sleeveInsetWidth/2])
            cube ([axleRibWidth,coneBigR*2,2], center=true) ;
        
    }   
}
unsigned int freqSerialRead = 0;
unsigned int frequency = 440;

const unsigned int syncPinInput = 2; // can be only 2, because interrupt 0 is used
const unsigned int pulsePinOut = 8;

const unsigned short cyclesPerSync = 10;
volatile unsigned short syncCounter = cyclesPerSync;
volatile unsigned short pulseState;

#define PRESCALE_1      1
#define PRESCALE_8      2
#define PRESCALE_64     3
#define PRESCALE_256    4
#define PRESCALE_1024   5



void setup()
{
    Serial.begin(9600);
    pinMode(syncPinInput, INPUT);
    pinMode(pulsePinOut, OUTPUT);
    attachInterrupt(0, checkSyncISR, RISING);  // zero interrupt is tied to Digital pin 2

    Serial.print("Starting with frequency=");
    Serial.println(frequency); 

    startTimer(0);
}


void loop()
{
    // read frequency input
    readSerial();

}


void checkSyncISR()
{
    //cli();
    
    if (syncCounter > cyclesPerSync)
    {
        syncCounter = 0;
        TCNT1  = 0; // reset timer counter
        pulseState = HIGH;
        digitalWrite(pulsePinOut, HIGH);
    }

    //sei();
}

ISR(TIMER1_COMPA_vect)
{
    ++syncCounter;
    if (pulseState == HIGH)
        pulseState = LOW;
    else
        pulseState = HIGH;
    digitalWrite(pulsePinOut, pulseState);        
}


void startTimer(unsigned int counter)
{
    //(timer speed (Hz)) = (Arduino clock speed (16MHz)) / prescaler
    
    TCCR1A = 0;
    TCCR1B = 0;
    TCNT1  = counter;
        
    OCR1A = 284; //284 cycles results in 880Hz with 64 prescaler
    
    // turn on CTC mode
    //TCCR1B |= (1 << WGM12);
    // Set CS10 and CS12 bits for 1024 prescaler
    //TCCR1B |= (1 << CS12) | (1 << CS10);  
    TCCR1B = _BV (WGM12) | PRESCALE_64;
    
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);
}


void readSerial()
{
    while(Serial.available() > 0)
    {
        char tmpChar = Serial.read();

        if (isDigit(tmpChar))
        {
            freqSerialRead =  (freqSerialRead * 10 ) + tmpChar - '0';
        }

        if (tmpChar == '\n' || tmpChar == '\r')
        {
            Serial.print(freqSerialRead);
            Serial.println(" Hz Set O.K");
            frequency = freqSerialRead; 
            freqSerialRead = 0; 
        }
    }
}


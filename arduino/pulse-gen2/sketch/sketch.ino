/**
* Square wave pulse generator
* How to use it: Send desired frequency in Hz using serial port Baud = 9600 8,N,1
*                Send 's' to stop the pulse generator.
* Note: CR or LF must be send as a line termination.
*
* Kyle M. Douglass, 2016
* Based on original code from Elimelec
* http://elimelecsarduinoprojects.blogspot.ch/2014/06/arduino-square-wave-pulse-generator.html
*/

unsigned int freq   = 0; //frequency in Hz
unsigned int pinOut = 8;
bool turnOff        = false;

void setup(){

  Serial.begin(9600);//setup serial communication

}

void loop(){
  
  while(Serial.available() > 0){
      
     char tmpChar = Serial.read(); //read incoming character from serial port
  
     if(tmpChar == 's')
     {
       turnOff = true;
       Serial.println(" Stopping pulse sequence.");
       noTone(pinOut);
     }
      
     if(isDigit(tmpChar))
     {
       turnOff = false;
       freq =  (freq  * 10 ) + tmpChar - '0'; // convert from ascii code to it's corresponding number; '0' == DEC 48
     }
     
     if(tmpChar == '\n' || tmpChar == '\r'){ //if CR or LF was received 
       
       if(!turnOff)
       {
         Serial.print(freq);
         Serial.println(" Hz Set O.K");
         tone(pinOut, freq); //set current frequency
       }
       
       freq = 0; // clear freq
     }
     
  }
   
}
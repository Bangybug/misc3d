/**
* How to use it: Send desired frequency in Hz using serial port Baud = 9600 8,N,1
*                Send 's' to stop the pulse generator.
* Note: CR or LF must be send as a line termination.
*/

unsigned int freq = 0; //frequency in Hz
unsigned int frequency = 0;
const unsigned int pinOut = 8;
const int buttonPin = 2;
const int syncPin = 3;
bool turnOff = false;

void setup()
{
    Serial.begin(9600);//setup serial communication
    pinMode(buttonPin, INPUT);
    pinMode(syncPin, INPUT);
}

void loop()
{
	while(Serial.available() > 0)
	{
		char tmpChar = Serial.read();
		
		if (tmpChar == 's')
		{
			turnOff = true;
			Serial.println("Stopping pulse sequence.");
			freq = 0;
			noTone(pinOut);
		}

		if (isDigit(tmpChar))
		{
			turnOff = false;
			freq =  (freq  * 10 ) + tmpChar - '0';
		}

		if (tmpChar == '\n' || tmpChar == '\r')
		{
			if (!turnOff)
			{
				Serial.print(freq);
				Serial.println(" Hz Set O.K");
				tone(pinOut, freq);
			}
      frequency = freq; 
      freq = 0; 
		}
	}

	if (digitalRead(buttonPin) == HIGH)
	{
		noTone(pinOut);
   	if (pulseIn(syncPin,HIGH,2000) > 0)
		{
      delayMicroseconds(1136); // half period of 440 hz
  		tone(pinOut, frequency);
		}
	}
}

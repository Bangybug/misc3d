unsigned int tmpSerialRead = 0;
unsigned int indexNumber = 0;

unsigned int counters[4]; 
const unsigned int totalCounters = 4;

const unsigned int pulsePinOut = 8;


#define PRESCALE_1      1
#define PRESCALE_8      2
#define PRESCALE_64     3
#define PRESCALE_256    4
#define PRESCALE_1024   5



void setup()
{
    Serial.begin(9600);
    pinMode(pulsePinOut, OUTPUT);

    counters[0] = 84; // frequency, 23.6khz
    counters[1] = 16; // microseconds of high pulse
    counters[2] = 1;  // TODO step for frequency
    counters[3] = 1;  // TODO step for microseconds
    
    startTimer1();
}


void loop()
{
    // read frequency input
    readSerial();

}

ISR(TIMER1_COMPA_vect)
{
    digitalWrite(pulsePinOut, HIGH);
    delayMicroseconds(counters[1]);
    digitalWrite(pulsePinOut, LOW);
}


void startTimer1()
{
    //(timer speed (Hz)) = (Arduino clock speed (16MHz)) / prescaler
    
    TCCR1A = 0;
    TCCR1B = 0;
    TCNT1  = 0;
        
    OCR1A = counters[0]; //26.3
    
    // turn on CTC mode
    //TCCR1B |= (1 << WGM12);
    // Set CS10 and CS12 bits for 1024 prescaler
    //TCCR1B |= (1 << CS12) | (1 << CS10);  
    TCCR1B = _BV (WGM12) | PRESCALE_8;
    
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);
}




void readSerial()
{
    while(Serial.available() > 0)
    {
        char tmpChar = Serial.read();

        if (isDigit(tmpChar))
        {
            tmpSerialRead =  (tmpSerialRead * 10 ) + tmpChar - '0';
        }
        else if (tmpChar == '\n' || tmpChar == '\r')
        {
            if (tmpSerialRead != 0)
            {
               if (indexNumber == 0)
                  OCR1A = tmpSerialRead;            
 
              Serial.print(tmpSerialRead);
              Serial.print(" sent to ");
              Serial.println( indexNumber );
              counters[indexNumber] = tmpSerialRead;
              tmpSerialRead = 0;
            }   
        }
        else
        {
            indexNumber = constrain( tmpChar - 'a', 0, totalCounters);
            
            Serial.print("Index number set ");
            Serial.println(indexNumber); 
        }
            
    }
}

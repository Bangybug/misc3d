unsigned int tmpSerialRead = 0;
unsigned int indexNumber = 0;
unsigned int counters[4]; 

const unsigned int syncPinInput = 2; // can be only 2, because interrupt 0 is used
const unsigned int pulsePinOut = 8;

const unsigned short cyclesPerSync = 10;
volatile unsigned short syncCounter = cyclesPerSync;

#define PRESCALE_1      1
#define PRESCALE_8      2
#define PRESCALE_64     3
#define PRESCALE_256    4
#define PRESCALE_1024   5



void setup()
{
    Serial.begin(9600);
    pinMode(syncPinInput, INPUT);
    pinMode(pulsePinOut, OUTPUT);
    attachInterrupt(0, checkSyncISR, RISING);  // zero interrupt is tied to Digital pin 2

    counters[0] = 84; //23.6khz
    counters[1] = 32; //16us 
    counters[2] = 0;  //0s delay  
    counters[2] = 16;
    
    startTimer1();
    //startTimer2();
}


void loop()
{
    // read frequency input
    readSerial();

}


void checkSyncISR()
{
    if (syncCounter > cyclesPerSync)
    {
        syncCounter = 0;
        TCNT1 = counters[2]; // reset timer counter
        //TCNT2 = 0;
        digitalWrite(pulsePinOut, HIGH);
        delayMicroseconds(counters[3]);
        digitalWrite(pulsePinOut, LOW);
    }
}

ISR(TIMER1_COMPA_vect)
{
    ++syncCounter;
    digitalWrite(pulsePinOut, HIGH);
    delayMicroseconds(counters[3]);
    digitalWrite(pulsePinOut, LOW);
}

//ISR(TIMER2_COMPA_vect)
//{
//    digitalWrite(pulsePinOut, HIGH);  
//}


void startTimer1()
{
    //(timer speed (Hz)) = (Arduino clock speed (16MHz)) / prescaler
    
    TCCR1A = 0;
    TCCR1B = 0;
    TCNT1  = 0;
        
    OCR1A = counters[0]; //26.3
    
    // turn on CTC mode
    //TCCR1B |= (1 << WGM12);
    // Set CS10 and CS12 bits for 1024 prescaler
    //TCCR1B |= (1 << CS12) | (1 << CS10);  
    TCCR1B = _BV (WGM12) | PRESCALE_8;
    
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);
}

void startTimer2()
{
  TCCR2A = 0;// set entire TCCR2A register to 0
  TCCR2B = 0;// same for TCCR2B
  TCNT2  = 0;//initialize counter value to 0
  
  OCR2A = counters[1];
  
  TCCR1B = _BV (WGM12) | PRESCALE_8;
  // enable timer compare interrupt
  TIMSK2 |= (1 << OCIE2A);
}


void readSerial()
{
    while(Serial.available() > 0)
    {
        char tmpChar = Serial.read();

        if (isDigit(tmpChar))
        {
            tmpSerialRead =  (tmpSerialRead * 10 ) + tmpChar - '0';
        }
        else if (tmpChar == '\n' || tmpChar == '\r')
        {
            if (tmpSerialRead != 0)
            {
              if (indexNumber < 2)
              {
                if (indexNumber == 0)
                  OCR1A = tmpSerialRead;
                else
                  OCR2A = tmpSerialRead;   
              }
 
              Serial.print(tmpSerialRead);
              Serial.print(" sent to ");
              Serial.println( indexNumber );
              counters[indexNumber] = tmpSerialRead;
              tmpSerialRead = 0;
            }   
        }
        else
        {
            indexNumber = constrain( tmpChar - 'a', 0, 3);
            
            Serial.print("Index number set ");
            Serial.println(indexNumber); 
        }
            
    }
}


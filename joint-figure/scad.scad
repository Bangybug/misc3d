discRadius = 4;
discHoleHeight = 1.2;  

potL = 4.8; 
potW = 4.5; 
potH = 2.55;  
potSpacingXY = 0.25;

wireTunnelL = 1.3;
wireTunnelW = 0.7;
wireTunnelBottomL = 1.6;
wireTunnelDist = 0.65;

potPadWidth = 2.4;
potPadHeight = 0.6;

length = 12;
width = 12;
height = discHoleHeight + potH + potPadHeight;


module cradlePot()
{         
    difference() {
        cube ([length,
               width,
               height],
               center=true) ;
        
        translate ([0,0, height/2 - discHoleHeight/2 ]) {
            cylinder($fn = 128, h = discHoleHeight, r1 = discRadius, r2 = discRadius, center = true);
        }
        
        
        _potHouseL = potL+potSpacingXY*2;
        _potHouseW = potW+potSpacingXY*2;
        
        translate ([0,0, height/2 - discHoleHeight - potH/2 ]) {
            cube([ _potHouseL, _potHouseW, potH], center=true);
                       
            _potPadGapW = (_potHouseW-potPadWidth)/2;
            
            // pot pad (cut two gaps)
            translate([0, _potHouseW/2-_potPadGapW /2, -potH/2]) {
                cube([ _potHouseL, _potPadGapW, height], center=true);
            }
            translate([0, -_potHouseW/2+_potPadGapW /2, -potH/2]) {
                cube([ _potHouseL, _potPadGapW, height], center=true);
            }
            
            // wire tunnels
            translate([ -wireTunnelDist - wireTunnelL/2, _potHouseW/2 + wireTunnelW/2, -potPadHeight/2   ]) {
                cube([ wireTunnelL, wireTunnelW, potH+potPadHeight ], center=true);
            }
            translate([ wireTunnelDist + wireTunnelL/2, _potHouseW/2 + wireTunnelW/2, -potPadHeight/2   ]) {
                cube([ wireTunnelL, wireTunnelW, potH+potPadHeight ], center=true);
            }
            translate([ 0, -_potHouseW/2 - wireTunnelW/2, -potPadHeight/2   ]) {
                cube([ wireTunnelBottomL, wireTunnelW, potH+potPadHeight ], center=true);
            }
            
        }
    }
}


cradlePot();